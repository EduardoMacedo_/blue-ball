package src;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;
public class Ball {
    private Text scoreText;
    private Picture ball;
    private Brick[][] bricks;
    private Paddle paddle;
    private Background pic;
    private Direction direction = Direction.UP_RIGHT;
    private boolean inPlay = false;
    private double x = -1;
    private double y = 1;
    private int speed = 5;
    public static int points;

    public Ball(Background pic, Paddle paddle, Brick[][] bricks){
        this.pic = pic;
        this.paddle = paddle;
        this.bricks = bricks;
        ball = new Picture(390, paddle.getY() - Background.PADDING , "ball_15x15.png");
        ball.draw();
    }

     public void moving()  {

        while (!inPlay) {
            if (ball.getY() > pic.getHeight()) {
                Picture gameOver= new Picture(200,250,"Game-Over.png");
                gameOver.draw();
                ball.delete();
                inPlay = false;
                return;
            }
            boolean collBricks = collisionBricks();
            boolean collPaddle = collisionPaddle();
            boolean collWalls = collisionWall();
            if (!collBricks || !collPaddle || !collWalls) {
               try {
                    Thread.sleep(speed);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                switch (direction) {
                    case DOWN_RIGHT:
                        moveDownRight();
                        break;
                    case DOWN_LEFT:
                        moveDownLeft();
                        break;
                    case UP_RIGHT:
                        moveUpRight();
                        break;
                    case UP_LEFT:
                        moveUpLeft();
                        break;
                }
            }
        

         }
    }
    private void moveUpRight() {
        ball.translate(1 * x, -1 * y);
        direction = Direction.UP_RIGHT;
    }
    private void moveUpLeft() {
        ball.translate(-1 * x, -1 * y);
        direction = Direction.UP_LEFT;
    }
    private void moveDownRight() {
        ball.translate(1 * x, 1 * y);
        direction = Direction.DOWN_RIGHT;
    }
    private void moveDownLeft() {
        ball.translate(-1 * x, 1 * y);
        direction = Direction.DOWN_LEFT;
    }
    public boolean collisionBricks() {
        for (int i = 0; i < 10 ; i++) {
            for (int j = 0; j < 5; j++) {
                if (!bricks[i][j].getInGame()) {
                    continue;
                }
                boolean collisionW = ((ball.getX() + ball.getWidth() >= bricks[i][j].getX()) && (ball.getX() <= bricks[i][j].getX() + bricks[i][j].getWidth()));
                boolean collisionH = ((ball.getY() + ball.getHeight() >= bricks[i][j].getY()) && (ball.getY() <= bricks[i][j].getY() + bricks[i][j].getHeight()));
                if (collisionW && collisionH) {

                    bricks[i][j].delete();
                    bricks[i][j].removeBrick();
                    points+=5;

                    switch (direction) {
                        case UP_LEFT:
                            if (ball.getY() == bricks[i][j].getY() + bricks[i][j].getHeight()) {
                                direction = Direction.DOWN_LEFT;

                                break;
                            }
                            direction = Direction.UP_RIGHT;
                            break;
                        case UP_RIGHT:
                            if ((ball.getY() == bricks[i][j].getY() + bricks[i][j].getHeight())) {
                                direction = Direction.DOWN_RIGHT;
                                break;
                            }
                            direction = Direction.UP_LEFT;
                            break;
                        case DOWN_RIGHT:
                            if ((ball.getY() + ball.getHeight() == bricks[i][j].getY())) {
                                direction = Direction.UP_RIGHT;
                                break;
                            }
                            direction = Direction.DOWN_LEFT;
                            break;
                        case DOWN_LEFT:
                            if (ball.getY() + ball.getHeight() == bricks[i][j].getY()) {
                                direction = Direction.UP_LEFT;
                                break;
                            }
                            direction= Direction.DOWN_RIGHT;
                            break;
                    }
                    return true;
                }

            }
        }

        return false;
    }
    public boolean collisionPaddle() {
        if (ball.getY() + ball.getHeight() == paddle.getY() + Background.PADDING && (ball.getX() + ball.getWidth() > paddle.getX() && ball.getX() < paddle.getX() + (paddle.getWidth()* 0.16))) {

            direction = Direction.UP_LEFT;
            x = 1;
            y = 0.5;
            speed = 4;
            return true;
        }
        if (ball.getY() + ball.getHeight() == paddle.getY() + Background.PADDING && (ball.getX() + ball.getWidth() > paddle.getX() + (paddle.getWidth()* 0.16) && ball.getX() < paddle.getX() + (paddle.getWidth()* 0.33))) {

            direction = Direction.UP_LEFT;
            x = 1;
            y = 1;
            speed = 5;
            return true;

        }
        if (ball.getY() + ball.getHeight() == paddle.getY() + Background.PADDING && (ball.getX() + ball.getWidth() > paddle.getX() + (paddle.getWidth()* 0.33) && ball.getX() < paddle.getX() + (paddle.getWidth()* 0.5))) {

            direction = Direction.UP_LEFT;
            x = 0.5;
            y = 1;
            speed = 4;
            return true;

        }
        if (ball.getY() + ball.getHeight() == paddle.getY() + Background.PADDING && (ball.getX() + ball.getWidth() > paddle.getX() + (paddle.getWidth() * 0.5) && ball.getX() < paddle.getX() + (paddle.getWidth() * 0.67))) {

            direction = Direction.UP_RIGHT;
            x = 0.5;
            y = 1;
            speed = 4;
                    return true;

            }
        if (ball.getY() + ball.getHeight() == paddle.getY() + Background.PADDING && (ball.getX() + ball.getWidth() > paddle.getX() + (paddle.getWidth() * 0.67) && ball.getX() < paddle.getX() + (paddle.getWidth() * 0.84))) {

            direction = Direction.UP_RIGHT;
            x = 1;
            y = 1;
            speed = 5;
            return true;

        }
        if (ball.getY() + ball.getHeight() == paddle.getY() + Background.PADDING && (ball.getX() + ball.getWidth() > paddle.getX() + (paddle.getWidth()* 0.84) && ball.getX() < paddle.getX() + (paddle.getWidth()))) {

            direction = Direction.UP_RIGHT;
            x = 1;
            y = 0.5;
            speed = 4;
            return true;

        }
        return false;
        }


    public boolean collisionWall() {
        if (ball.getX() <= pic.getX() || ball.getX() + ball.getWidth() >= pic.getWidth() + Background.PADDING) {

            switch (direction) {
                case DOWN_LEFT:
                    direction = Direction.DOWN_RIGHT;
                    break;
                case DOWN_RIGHT:
                    direction = Direction.DOWN_LEFT;
                    break;
                case UP_LEFT:
                    direction = Direction.UP_RIGHT;
                    break;
                case UP_RIGHT:
                    direction = Direction.UP_LEFT;
                    break;
            }
            return true;
        }
        if (ball.getY() <= Background.PADDING) {
            switch (direction) {
                case UP_RIGHT:
                    direction = Direction.DOWN_RIGHT;
                    break;
                case UP_LEFT:
                    direction = Direction.DOWN_LEFT;
                    break;
            }
            return true;
        }
        return false;
    }
    public int getPoints(){
        return points;
    }

    public int getSpeed() {
        return speed;
    }
}