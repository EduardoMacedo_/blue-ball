package src;

import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

    public class PaddleHandler implements KeyboardHandler {
        private Paddle paddle;
        private Ball ball;
        public PaddleHandler( Paddle paddle, Ball ball) {
            this.ball = ball;
            this.paddle = paddle;
        }
        @Override
        public void keyPressed(KeyboardEvent keyboardEvent) {
            switch (keyboardEvent.getKey()) {
                case KeyboardEvent.KEY_LEFT:
                    paddle.moveLeft();
                    break;
                case KeyboardEvent.KEY_RIGHT:
                    paddle.moveRight();
                    break;
                case KeyboardEvent.KEY_Q:
                    System.exit(0);
                    break;
                case KeyboardEvent.KEY_SPACE:
                        ball.moving();
                        break;
            }
        }
        @Override
        public void keyReleased(KeyboardEvent keyboardEvent) {

        }
    }


