package src;

import org.academiadecodigo.simplegraphics.pictures.Picture;
public class Background {
    public static final int PADDING = 10;
    public static final int CELLSIZE = 30;
    private Picture background;

    public Background(){
        background = new Picture(PADDING,PADDING, "fundo.png");
        background.draw();
    }

    public int getX(){
        return this.background.getX();
    }
    public int getY(){
        return this.background.getY();
    }
    public int getMaxX(){return background.getMaxX();}
    public int getMaxY(){return background.getMaxY();}
    public int getWidth() {return background.getWidth();}
    public int getHeight() {return background.getHeight();}
    public int maxCol(){return (background.getMaxX() - PADDING)/CELLSIZE;}
    public int maxRow(){return (background.getMaxY() - PADDING)/CELLSIZE;}
}


