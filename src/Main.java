package src;

public class Main {
    public static void main(String[] args) {


        Background pic = new Background();
        Paddle paddle = new Paddle(pic);
        BrickFactory bf = new BrickFactory(10, 5);
        Brick[][] bricks = bf.getBricks();
        Ball ball = new Ball(pic,paddle,bricks);
        PaddleHandler paddleHandler = new PaddleHandler(paddle, ball);
        KeyboardManager.initKeyEvents(paddleHandler);
        ball.moving();
    }
}
